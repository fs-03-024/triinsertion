function insertionSort(arr) {
    // Iterate over the array starting from the second element
    for (let i = 1; i < arr.length; i++) {
      // Choose the current element
      const curr = arr[i];
  
      // Iterate over the sorted sequence from 0 to i-1
      for (let j = i - 1; j >= 0 && arr[j] > curr; j--) {
        // Shift the elements to the right to make room for the current element
        arr[j + 1] = arr[j];
      }
  
      // Insert the current element into the sorted sequence
      arr[j + 1] = curr;
    }
  
    // Return the sorted array
    return arr;
  }